#!/usr/bin/env bash
#title		:transfersh_install.sh
#description	:Install script and add alias for transfersh
#author		:Valeriu Stinca
#email		:sys@0hz.io
#date		:20171112
#version	:1
#notes		:
#===================
curl --create-dirs -s https://gitlab.com/strategiczone/initial_install/-/raw/master/transfersh-alias/transfersh-alias.sh -o ~/.transfersh/transfersh-alias.sh
read -p "Press Enter for https://tranfer.sh server or your enter your tranfersh server? https://" TS_SERVER
TS_SERVER=${TS_SERVER:-transfer.sh}
sed -i "/^TS_SERVER=/c\TS_SERVER=\"https://${TS_SERVER}\"" ~/.transfersh/transfersh-alias.sh
alias trs="~/.transfersh/transfersh-alias.sh "
echo 'alias trs="~/.transfersh/transfersh-alias.sh "' | tee -a ~/.aliasrc
echo "All is OK! Please check your TS_SERVER in ~/.transfersh/transfersh-alias.sh"
